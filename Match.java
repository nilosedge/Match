//  InteractApplet.java
//  by David Wellman

import java.awt.Color;
import java.awt.Container;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
//import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.event.MouseInputAdapter;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.JOptionPane;
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;



class GraphicalObjectListener extends MouseInputAdapter implements ActionListener
{
    public void actionPerformed(ActionEvent e)
    {}
}    

abstract class GeoShape extends JComponent
{
    static Cursor in = new Cursor (Cursor.HAND_CURSOR);
    static Cursor out = new Cursor (Cursor.DEFAULT_CURSOR);
    int prevX, prevY;
    protected Color c;
    protected Rectangle r;
	Point centerpoint;
    
    public GeoShape(int _xposit, int _yposit, int _width, int _height, Color _c)
    {
		c= _c;
		r = new Rectangle(_xposit, _yposit, _width, _height);
		centerpoint = new Point(getX() + (1/2 * _width), getY() + (1/2 * _height));

		addMouseMotionListener(new GraphicalObjectListener()
		{
	    	public void mouseDragged(MouseEvent e)
	    	{
 				setLocation(e.getX() + (getX() - prevX), e.getY() + (getY() - prevY));
 				getParent().repaint();
	    	}
		
		});
		addMouseListener(new GraphicalObjectListener()
		{
	    	public void mouseClicked(MouseEvent e)
	    	{
			}
	    	public void mouseEntered(MouseEvent e)
	    	{
				setCursor(in);
	    	}
	    	public void mouseExited(MouseEvent e)
	    	{
				setCursor(out);
	    	}
	    	public void mousePressed(MouseEvent e)
	    	{
				prevX = e.getX();
				prevY = e.getY();
	    	}
	    	public void mouseReleased(MouseEvent e)
	    	{
	    	}
		
		});
	}

	public void relocate(Graphics g, int _x, int _y)
	{
		move(g, _x, _y);
	}

    public Rectangle getBounds()
    {
		return r;
    }
    

    public void addNotify()
 	{
   		setBounds(r);
    	super.addNotify();
    }


    public Dimension getPreferredSize()
    {
		return new Dimension(r.width, r.height);
    }
    
    public void paint(Graphics g)
    {
		draw(g);
    }

	public Color getColor()
	{
		return c;
	}

	public void setColor(Color color)
	{
		c = color;
	}
    

    /** Initializes the bounding rectangle and color of a Geoshape.
    */
    protected void undraw(Graphics g)
    {
		g.clearRect(r.x, r.y, r.width, r.height);
    }
    /** Reposistions the current GeoShape to the specified center coordinante
    */
    public void move(Graphics g,int _x,int _y)
    {
		undraw(g);
		reposition(g, _x, _y);
		draw(g);
    }
    protected void reposition(Graphics g, int _x, int _y)
    {
		g.setColor(c);
 		r.x = _x - r.width/2;
 		r.y = _y - r.height/2;
		centerpoint.setLocation(_x, _y);
    }
	
    abstract protected void draw(Graphics g);

	abstract protected char getType();
}

class GeoRectangle extends GeoShape
{
    /** Constructs a new <tt>GeoRectangle</tt> with the specified coordinates and color
    */
    public GeoRectangle(int _xposit, int _yposit, int _width, int _height, Color _c)
    {
	super(_xposit, _yposit, _width, _height, _c);
    } 
    protected void draw(Graphics g)
    {
	g.setColor(c);
	g.fillRect(r.x, r.y, r.width, r.height);
	//g.fillRect(0, 0, getWidth(), getHeight());
    }

	protected char getType()
	{
		return 'r';
	}
}

class GeoCircle extends GeoShape
{
	
    /** Constructs a new <tt>GeoCircle</tt> with the specified center coordinate, radius, and color
    */
    public GeoCircle(int _xposit, int _yposit, int _width, int _height, Color _c)
    {
 		super(_xposit, _yposit, _width, _height, _c);
    }
    protected void draw(Graphics g)
    {
		g.setColor(c);
 		g.fillOval(r.x, r.y, r.width, r.height);
    }

	protected char getType()
	{
		return 'c';
	}
	    
}

class GeoPolygon extends GeoShape
{

    protected int[] xarray, yarray;
    protected int minX = Integer.MAX_VALUE;
	protected int minY = Integer.MAX_VALUE;
	protected int sides;
    protected Polygon p;
    /** Constructs a new <tt>GeoTriangle</tt> with the specified verticies and color
    */
    public GeoPolygon(int sides, Color _c)
    {
		super(0, 0, 0, 0, _c);
		this.sides = sides;
		xarray = new int[sides];
		yarray = new int[sides];

		double angle = 2 * Math.PI / sides;

		for (int i = 0; i < sides; i++)
		{
	    	xarray[i] = (int) Math.round(100 + 25 * Math.sin(angle * i));
	    	yarray[i] = (int) Math.round(100 + 25 * Math.cos(angle * i));
			minX = (xarray[i] < minX) ? xarray[i] : minX;
			minY = (yarray[i] < minY) ? yarray[i] : minY;
		}
		
		for (int i = 0; i < sides; i++)
		{
			xarray[i] -= minX;
			yarray[i] -= minY;
		}

		p=new Polygon(xarray, yarray, sides);
		r = p.getBounds();
		centerpoint=new Point(r.x + r.width/2, r.y + r.height/2);
    }
    protected void draw(Graphics g)
    {
		g.setColor(c);
		g.fillPolygon(p);
    }
    protected void reposition(Graphics g, int _x, int _y)
    {
		g.setColor(c);
  		int xdistance = _x - centerpoint.x;
  		int ydistance = _y - centerpoint.y;
		for (int i = 0; i < xarray.length; i++)
		{
		    p.xpoints[i] = p.xpoints[i]+xdistance;
		    p.ypoints[i] = p.ypoints[i]+ydistance;
		}
		r = p.getBounds();
		centerpoint.setLocation(_x, _y);
    }

	protected char getType()
	{
		if (sides == 3)
			return 't';
		else
			return 'p';
	}
}

class MatchShapeApplet extends JApplet
{
	GeoShape shape;
	GeoShape fixedCircle = new GeoCircle(0, 0, 40, 40, Color.gray);
	GeoShape fixedTriangle = new GeoPolygon(3, Color.gray);
	GeoShape fixedRectangle = new GeoRectangle(0, 0, 40, 40, Color.gray);
	GeoShape fixedPentagon = new GeoPolygon(5, Color.gray);
	Timer timer;
	private static final int EXTERNAL_BUFFER_SIZE = 128000;
	private java.util.Random random = new java.util.Random();
	Container cp;


	public void playClip(String strFilename)
	{
		File	soundFile = new File(strFilename);
	
		AudioInputStream audioInputStream = null;
		try
		{
			audioInputStream = AudioSystem.getAudioInputStream(soundFile);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}

		AudioFormat	audioFormat = audioInputStream.getFormat();

		SourceDataLine	line = null;
		DataLine.Info	info = new DataLine.Info(SourceDataLine.class,
							 audioFormat);
		try
		{
			line = (SourceDataLine) AudioSystem.getLine(info);
			line.open(audioFormat);
		}
		catch (LineUnavailableException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}

		line.start();

		int nBytesRead = 0;
		byte[]	abData = new byte[EXTERNAL_BUFFER_SIZE];
		while (nBytesRead != -1)
		{
			try
			{
				nBytesRead = audioInputStream.read(abData, 0, abData.length);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			if (nBytesRead >= 0)
			{
				int	nBytesWritten = line.write(abData, 0, nBytesRead);
			}
		}

		line.drain();

		line.close();

	}

	private void removeListeners(JComponent component)
	{
		MouseMotionListener listeners[] = component.getMouseMotionListeners();
		for (int i = 0; i < listeners.length; i++)
			component.removeMouseMotionListener(listeners[i]);
	}

	private Color randomColor()
	{
		Color color = Color.red;

		switch (random.nextInt(6))
		{
			case 0:
				color = Color.red;
				break;
			case 1:
				color = Color.orange;
				break;
			case 2:
				color = Color.yellow;
				break;
			case 3:
				color = Color.green;
				break;
			case 4:
				color = Color.blue;
				break;
			case 5:
				color = new Color(255, 0, 255);
				break;
		}

		return color;
	}

	private GeoShape randomShape()
	{
		GeoShape gs = null;
		switch (random.nextInt(4))
		{
			case 0:
				gs = new GeoRectangle(0, 0, 40, 40, randomColor());
				break;
			case 1:
				gs = new GeoCircle(0, 0, 40, 40, randomColor());
				break;
			case 2:
				gs = new GeoPolygon(3, randomColor());
				break;
			case 3:
				gs = new GeoPolygon(5, randomColor());
				break;
		}

		return gs;
	}
	
	private void matchShape()
	{
		if (shape != null)
		{
		switch (shape.getType())
		{
			case 'c':
				if (shape.getX() == fixedCircle.getX() && shape.getY() < fixedCircle.getY() + 5
					&& shape.getY() > fixedCircle.getY() - 5)
				{
					shape.setVisible(false);
					shape = null;
					//playClip("Toilet.wav");
				}
				break;
			
			case 'p':
				if (shape.getX() == fixedPentagon.getX() && shape.getY() < fixedPentagon.getY() + 5
					&& shape.getY() > fixedPentagon.getY() - 5)
				{
					shape.setVisible(false);
					shape = null;
					//playClip("Toilet.wav");
				}
				break;
			
			case 'r':
				if (shape.getX() == fixedRectangle.getX() && shape.getY() < fixedRectangle.getY() + 5
					&& shape.getY() > fixedRectangle.getY() - 5)
				{
					shape.setVisible(false);
					shape = null;
					//playClip("Toilet.wav");
				}
				break;
			case 't':
				if (shape.getX() == fixedTriangle.getX() && shape.getY() < fixedTriangle.getY() + 5
					&& shape.getY() > fixedTriangle.getY() - 5)
				{
					shape.setVisible(false);
					shape = null;
					//playClip("Toilet.wav");
				}
		}
		}
	}
		
	public void init()
	{
		

		removeListeners(fixedTriangle);
		removeListeners(fixedRectangle);
		removeListeners(fixedPentagon);
		removeListeners(fixedCircle);

		cp = getContentPane();
		cp.setSize(350,300);
		cp.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		setBackground(Color.white);
		

		timer = new Timer(10, new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (shape != null)
					matchShape();
				else
				{
					shape = randomShape();
					repaint();
				}
			}
		});

		cp.setBackground(Color.WHITE);	
		cp.add(Box.createVerticalStrut(300));
		//cp.add(Box.createHorizontalStrut(250));
		shape = randomShape();
		cp.add(shape);
		cp.add(fixedCircle);
		cp.add(fixedTriangle);
		cp.add(fixedRectangle);
		cp.add(fixedPentagon);
		shape.setLocation(105, 5);
		fixedCircle.setLocation(30, 250);
		fixedTriangle.setLocation(80, 250);
		fixedRectangle.setLocation(130, 250);
		fixedPentagon.setLocation(180, 250);
		timer.start();
    }
	
	public void paint(Graphics g)
	{
		super.paint(g);
		cp.removeAll();
		cp.add(Box.createVerticalStrut(300));
		//cp.add(Box.createHorizontalStrut(250));
		cp.add(shape);
		cp.add(fixedCircle);
		cp.add(fixedTriangle);
		cp.add(fixedRectangle);
		cp.add(fixedPentagon);
		shape.setLocation(105, 5);
		fixedCircle.setLocation(30, 250);
		fixedTriangle.setLocation(80, 250);
		fixedRectangle.setLocation(130, 250);
		fixedPentagon.setLocation(180, 250);
	}

}
	
/** Begining of client code.
*/

class MatchColorApplet extends JApplet
{
	private GeoShape shape = null;
	GeoShape fixedCircle = new GeoCircle(0, 0, 40, 40, Color.red);
	GeoShape fixedTriangle = new GeoPolygon(3, Color.blue);
	GeoShape fixedRectangle = new GeoRectangle(0, 0, 40, 40, Color.green);
	GeoShape fixedPentagon = new GeoPolygon(5, Color.yellow);
	Timer timer;
	private static final int EXTERNAL_BUFFER_SIZE = 128000;
	private java.util.Random random = new java.util.Random();
	Container cp;

	public void playClip(String strFilename)
	{
		File	soundFile = new File(strFilename);
	
		AudioInputStream audioInputStream = null;
		try
		{
			audioInputStream = AudioSystem.getAudioInputStream(soundFile);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}

		AudioFormat	audioFormat = audioInputStream.getFormat();

		SourceDataLine	line = null;
		DataLine.Info	info = new DataLine.Info(SourceDataLine.class,
							 audioFormat);
		try
		{
			line = (SourceDataLine) AudioSystem.getLine(info);
			line.open(audioFormat);
		}
		catch (LineUnavailableException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}

		line.start();

		int nBytesRead = 0;
		byte[]	abData = new byte[EXTERNAL_BUFFER_SIZE];
		while (nBytesRead != -1)
		{
			try
			{
				nBytesRead = audioInputStream.read(abData, 0, abData.length);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			if (nBytesRead >= 0)
			{
				int	nBytesWritten = line.write(abData, 0, nBytesRead);
			}
		}

		line.drain();

		line.close();

	}

	private void removeListeners(JComponent component)
	{
		MouseMotionListener listeners[] = component.getMouseMotionListeners();
		for (int i = 0; i < listeners.length; i++)
			component.removeMouseMotionListener(listeners[i]);
	}

	private Color randomColor()
	{
		Color color = Color.red;

		switch (random.nextInt(6))
		{
			case 0:
				color = Color.red;
				break;
			case 1:
				color = Color.orange;
				break;
			case 2:
				color = Color.yellow;
				break;
			case 3:
				color = Color.green;
				break;
			case 4:
				color = Color.blue;
				break;
			case 5:
				color = new Color(255, 0, 255);
				break;
		}

		return color;
	}

	private GeoShape randomShape()
	{
		GeoShape gs = null;
		switch (random.nextInt(4))
		{
			case 0:
				gs = new GeoRectangle(0, 0, 40, 40, randomColor());
				break;
			case 1:
				gs = new GeoCircle(0, 0, 40, 40, randomColor());
				break;
			case 2:
				gs = new GeoPolygon(3, randomColor());
				break;
			case 3:
				gs = new GeoPolygon(5, randomColor());
				break;
		}

		return gs;
	}
	
	private Color fixedColor()
	{
		Color color = null;
		switch (random.nextInt(4))
		{
			case 0:
				color = fixedRectangle.getColor();
				break;
			case 1:
				color = fixedTriangle.getColor();
				break;
			case 2:
				color = fixedCircle.getColor();
				break;
			case 3:
				color = fixedPentagon.getColor();
				break;
		}
		return color;
	}

	private void matchColor()
	{
		if (shape != null)
		{
				if (shape.getX() < fixedCircle.getX() + 5 && shape.getX() > fixedCircle.getX() - 5 && shape.getY() < fixedCircle.getY() + 5
					&& shape.getY() > fixedCircle.getY() - 5 && shape.getColor().equals(fixedCircle.getColor()))
				{
					shape.setVisible(false);
					shape = null;
					//playClip("Toilet.wav");
				}
			
				else if (shape.getX() < fixedPentagon.getX() + 5 && shape.getX() > fixedPentagon.getX() - 5 && shape.getY() < fixedPentagon.getY() + 5
					&& shape.getY() > fixedPentagon.getY() - 5 && shape.getColor().equals(fixedPentagon.getColor()))
				{
					shape.setVisible(false);
					shape = null;
					//playClip("Toilet.wav");
				}
			
				else if (shape.getX() < fixedRectangle.getX() + 5 && shape.getX() > fixedRectangle.getX() - 5 && shape.getY() < fixedRectangle.getY() + 5
					&& shape.getY() > fixedRectangle.getY() - 5 && shape.getColor().equals(fixedRectangle.getColor()))
				{
					shape.setVisible(false);
					shape = null;
					//playClip("Toilet.wav");
				}
				else if (shape.getX() < fixedTriangle.getX() + 5 && shape.getX() > fixedTriangle.getX() - 5 && shape.getY() < fixedTriangle.getY() + 5
					&& shape.getY() > fixedTriangle.getY() - 5 && shape.getColor().equals(fixedTriangle.getColor()))
				{
					shape.setVisible(false);
					shape = null;
					//playClip("Toilet.wav");
				}
		}
	}
		
	public void init()
	{
		removeListeners(fixedTriangle);
		removeListeners(fixedRectangle);
		removeListeners(fixedPentagon);
		removeListeners(fixedCircle);

		cp = getContentPane();
		cp.setSize(350,300);
		cp.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		
		setBackground(Color.white);
		
		setBackground(Color.white);

		timer = new Timer(10, new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (shape != null)
					matchColor();
				else
				{
					shape = randomShape();
					repaint();
				}
			}
		});

		cp.setBackground(Color.WHITE);	
		cp.add(Box.createVerticalStrut(300));
		shape = randomShape();
		fixedCircle.setColor(randomColor());
		fixedTriangle.setColor(randomColor());
		fixedRectangle.setColor(randomColor());
		fixedPentagon.setColor(randomColor());
		shape.setColor(fixedColor());
		cp.add(shape);
		cp.add(fixedCircle);
		cp.add(fixedTriangle);
		cp.add(fixedRectangle);
		cp.add(fixedPentagon);
		shape.setLocation(105, 5);
		fixedCircle.setLocation(30, 250);
		fixedTriangle.setLocation(80, 250);
		fixedRectangle.setLocation(130, 250);
		fixedPentagon.setLocation(180, 250);
		timer.start();
    }
	
	public void paint(Graphics g)
	{
		super.paint(g);
		cp.removeAll();
		cp.add(Box.createVerticalStrut(300));
		fixedCircle.setColor(randomColor());
		fixedTriangle.setColor(randomColor());
		fixedRectangle.setColor(randomColor());
		fixedPentagon.setColor(randomColor());
		shape.setColor(fixedColor());
		cp.add(shape);
		cp.add(fixedCircle);
		cp.add(fixedTriangle);
		cp.add(fixedRectangle);
		cp.add(fixedPentagon);
		shape.setLocation(105, 5);
		fixedCircle.setLocation(30, 250);
		fixedTriangle.setLocation(80, 250);
		fixedRectangle.setLocation(130, 250);
		fixedPentagon.setLocation(180, 250);
	}
}

public class Match
{

	static Box mainBox = Box.createVerticalBox();
	static Box buttonBox = Box.createHorizontalBox();
	static JButton start = new JButton ("Match Color");
	static JMenuBar menu = new JMenuBar();
	static JMenu file = new JMenu("Choose Game");
	static JMenuItem MatchShapes, MatchColors;
	static JMenu help = new JMenu("Help");
	static MatchShapeApplet shapeApplet = new MatchShapeApplet();
	static MatchColorApplet colorApplet = new MatchColorApplet();
	static JFrame frame = new JFrame("Match the Shapes or Colors");
	static JOptionPane shapeDialog = new JOptionPane();
	static boolean shape = true;
    
	public static void main(String[] args)
    {
		frame.setSize(310, 310);
		final Container mainFrame = frame.getContentPane();
		frame.addWindowListener(new WindowAdapter()
		{
		    public void windowClosing(WindowEvent e)
		    {
				System.exit(0);
		    }
		});

	start.addActionListener(new ActionListener()
	{
	    public void actionPerformed(ActionEvent e)
	    {
			if (shape)
			{
				shapeApplet.timer.stop();
				frame.setVisible(false);
				mainFrame.removeAll();
				mainBox.removeAll();
				mainBox.add(colorApplet);
				mainBox.add(Box.createHorizontalStrut(270));
				mainBox.add(buttonBox);
				mainFrame.add(mainBox);
				colorApplet.init();
				frame.pack();
				frame.setVisible(true);
				start.setText("Match Shapes");
				shapeDialog.showMessageDialog(mainBox, "Now, match the colors.");
				shape = false;
			}
			else
			{
				colorApplet.timer.stop();
				frame.setVisible(false);
				mainFrame.removeAll();
				mainBox.removeAll();
				mainBox.add(shapeApplet);
				mainBox.add(Box.createHorizontalStrut(270));
				mainBox.add(buttonBox);
				mainFrame.add(mainBox);
				shapeApplet.init();
				frame.pack();
				frame.setVisible(true);
				start.setText("Match Colors");
				shapeDialog.showMessageDialog(mainBox, "Now, match the shapes.");
				shape = true;
			}
	    }
	});

	start.setBackground(Color.green);
	buttonBox.add(start);
	mainBox.add(shapeApplet);
	mainBox.add(Box.createHorizontalStrut(270));
	mainBox.add(buttonBox);
	mainFrame.add(mainBox);
	//shapeApplet.init();
	frame.pack();
	frame.setVisible(true);
	shapeDialog.showMessageDialog(mainBox, "Match the shapes.");
	}
}

